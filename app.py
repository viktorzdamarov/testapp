#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time
import requests
import json
import os
from datetime import datetime

class TestFailException(Exception):
    pass

errors: dict = {}


url = "https://crm-test.k8s.id-network.ru/api/oauth/token"

auth = {'username': 'crmuser@test.api', 'password': 'user123', 'grant_type': 'password'}


# Пост запрос с параметрами
def requestPost(json_query, headers):
    r = requests.post(url, data=json_query, headers=headers)
    return r

def requestGet(payload, headers):
    r = requests.post(url, params = payload, headers=headers)
    return r

# Словарь с переменными использующимися в запросах
context: dict = { }


headers_param = {"Accept": "application/json",
                 "Content-Type": "application/json",
                 "x-project-name": "crm",
                 "x-sysowner-id": "00000000-0000-0000-0002-000000000001"
                 }

answer = requestPost(json.dumps(auth), headers_param)
token = 'Bearer ' + answer.json()['access_token']
headers_param['authorization'] = token
url = "https://crm-test.k8s.id-network.ru/api/subject/list"
payload = {"with": ["type_classifier", "status_classifier", "names", "documents"]}
answer = requestGet(payload, headers_param)
subjects = answer.text
print(json.loads(subjects))
f = open( '1.txt', 'w')
f.write( subjects )
f.close()